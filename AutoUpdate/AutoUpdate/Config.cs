﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net;

namespace AutoUpdate
{
	public static class ConfigExt
	{

		public static AutoUpdateConfig GetConfig()
		{

			var json = File.ReadAllText(@"AutoUpdateConfig.json");

			return Newtonsoft.Json.JsonConvert.DeserializeObject<AutoUpdateConfig>(json);
		}
		public static AutoUpdateConfig GetConfig(string url)
		{
			using (var wc=new WebClient())
			{
				var json = wc.DownloadString(url);
				return Newtonsoft.Json.JsonConvert.DeserializeObject<AutoUpdateConfig>(json);
			}
		 
			
		}


		public static void SaveConfig(AutoUpdateConfig config)
		{
			var json = Newtonsoft.Json.JsonConvert.SerializeObject(config);
			File.Delete(@"AutoUpdateConfig.json");
			File.AppendAllText(@"AutoUpdateConfig.json", json);
		} 
	}

	public class AutoUpdateConfig
	{
		/// <summary>
		/// 
		/// </summary>
		public string Version { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string Url { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public List<string> Files { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string MainExe { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string Other { get; set; }

		 
	}
}
