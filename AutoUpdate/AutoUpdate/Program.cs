﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Forms;

namespace AutoUpdate
{
	public static class Program
	{
		//public static string Url;
		//public static string Exe;
		//public static string GetFileNameByUrl() {
		//	return Url.Substring(Url.LastIndexOf("/"));
		//}

		 public static AutoUpdateConfig config;
		/// <summary>
		/// 应用程序的主入口点。
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			config = ConfigExt.GetConfig();

			
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);



			Application.Run(new Form1());
		}

		

	}
}
