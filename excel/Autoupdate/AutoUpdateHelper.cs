﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net;

public static class AutoUpdateHelper
{

	public static AutoUpdateConfig GetConfig()
	{
		try
		{
			System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory);

			//if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + @"AutoUpdateConfig.json"))
			//{
			//	//如果配置文件不存在 直接从服务器下载

			//	using (var wc = new WebClient())
			//	{
			//		wc.DownloadFile(new Uri("https://98888888.gitee.io/smalltools/excel/Finpublish/AutoUpdateConfig.json"), "AutoUpdateConfig.json");
			//		 //wc.DownloadFileCompleted += Wc_DownloadFileCompleted;
			//	}
			//}

		}
		finally {

		}

		var json = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"AutoUpdateConfig.json");

		return Newtonsoft.Json.JsonConvert.DeserializeObject<AutoUpdateConfig>(json);
	}

	private static void Wc_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
	{
		var a = 1;
	}

	public static AutoUpdateConfig GetConfig(string url)
	{
		using (var wc = new WebClient())
		{
			return Newtonsoft.Json.JsonConvert.DeserializeObject<AutoUpdateConfig>(wc.DownloadString(new Uri(url)));
		}

	}
}

public class AutoUpdateConfig
{
	/// <summary>
	/// 
	/// </summary>
	public string Version { get; set; }
	/// <summary>
	/// 
	/// </summary>
	public string Url { get; set; }
	/// <summary>
	/// 
	/// </summary>
	public List<string> Files { get; set; }
	/// <summary>
	/// 
	/// </summary>
	public string MainExe { get; set; }
	/// <summary>
	/// 
	/// </summary>
	public string Other { get; set; }
}