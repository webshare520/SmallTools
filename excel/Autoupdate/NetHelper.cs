﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace Autoupdate
{
	public	class NetHelper
	{
	 

		/// <summary>
		/// 检查网络是否通
		/// </summary>
		/// <returns></returns>
		public static bool CheckNet()
		{

			Ping pingSender = new Ping();
			PingOptions options = new PingOptions();

			// Use the default Ttl value which is 128,
			// but change the fragmentation behavior.
			options.DontFragment = true;

			// Create a buffer of 32 bytes of data to be transmitted.
			string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
			byte[] buffer = Encoding.ASCII.GetBytes(data);
			int timeout = 120;
			PingReply reply = pingSender.Send("114.114.114.114", timeout, buffer, options);
			if (reply.Status == IPStatus.Success)
			{
				return true;
			}

			return false;
		}
	}
}
