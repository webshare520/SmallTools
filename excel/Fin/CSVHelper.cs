﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;

namespace Fin
{
	public static 	class CSVHelper
	{
		public static IWorkbook GetWorkbook(string file) {
			if (!File.Exists(file))
				throw new Exception("文件不存在！");
			if (!file.ToLower().Contains( ".csv"))
				throw new Exception("文件名格式不对，必须后缀名为csv");

			
			var strLines = File.ReadAllLines(file,Encoding.GetEncoding("gb2312"));

			var workBook = new HSSFWorkbook();

			var sheet = workBook.CreateSheet("表1");
			foreach (var item in strLines)
			{
				var values = item.Split(',');
				var row = sheet.CreateRow(sheet.LastRowNum+1);//创建新行
				for (int i = 0; i < values.Length; i++)
				{
					row.CreateCell(i).SetCellValue(values[i]);
				}
				 
			}

			return workBook;

		}

	}
}
