﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;

namespace Fin
{
	public partial class ConfigComboxComponent : Component
	{
		public ConfigComboxComponent()
		{
			InitializeComponent();

			Init();
		}

		public ConfigComboxComponent(IContainer container)
		{
			container.Add(this);

			InitializeComponent();

			Init();
		}

		void Init()
		{
			 
			this.cmbConfigs.Items.Clear();
			var  _configRoot = ConfigHelper.GetConfig();
			foreach (var item in _configRoot.TypeList)
			{
				this.cmbConfigs.Items.Add(item.Name);
			}
			this.cmbConfigs.Text = "";
			 


		}
	}
}
