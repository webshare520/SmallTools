﻿namespace Fin
{
	partial class ConfigFrm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbConfigs = new System.Windows.Forms.ComboBox();
			this.dgvConfigs = new System.Windows.Forms.DataGridView();
			this.btnAdd = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnKeyAdd = new System.Windows.Forms.Button();
			this.btnKeyDelete = new System.Windows.Forms.Button();
			this.btnDelte = new System.Windows.Forms.Button();
			this.btnCopyNew = new System.Windows.Forms.Button();
			this.KeyId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.KeyCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.KeyText = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.KeyType = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.KeyNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.dgvConfigs)).BeginInit();
			this.SuspendLayout();
			// 
			// cmbConfigs
			// 
			this.cmbConfigs.FormattingEnabled = true;
			this.cmbConfigs.Location = new System.Drawing.Point(12, 27);
			this.cmbConfigs.Name = "cmbConfigs";
			this.cmbConfigs.Size = new System.Drawing.Size(261, 23);
			this.cmbConfigs.TabIndex = 4;
			this.cmbConfigs.SelectedIndexChanged += new System.EventHandler(this.cmbConfigs_SelectedIndexChanged);
			// 
			// dgvConfigs
			// 
			this.dgvConfigs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvConfigs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.KeyId,
            this.KeyCheck,
            this.KeyText,
            this.KeyType,
            this.KeyNum});
			this.dgvConfigs.Location = new System.Drawing.Point(12, 144);
			this.dgvConfigs.Name = "dgvConfigs";
			this.dgvConfigs.RowTemplate.Height = 27;
			this.dgvConfigs.Size = new System.Drawing.Size(1247, 595);
			this.dgvConfigs.TabIndex = 5;
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(522, 26);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(75, 23);
			this.btnAdd.TabIndex = 7;
			this.btnAdd.Text = "添加";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(215, 115);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 23);
			this.btnSave.TabIndex = 7;
			this.btnSave.Text = "保存";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnKeyAdd
			// 
			this.btnKeyAdd.Location = new System.Drawing.Point(12, 115);
			this.btnKeyAdd.Name = "btnKeyAdd";
			this.btnKeyAdd.Size = new System.Drawing.Size(75, 23);
			this.btnKeyAdd.TabIndex = 8;
			this.btnKeyAdd.Text = "添加";
			this.btnKeyAdd.UseVisualStyleBackColor = true;
			this.btnKeyAdd.Click += new System.EventHandler(this.btnKeyAdd_Click);
			// 
			// btnKeyDelete
			// 
			this.btnKeyDelete.Location = new System.Drawing.Point(112, 115);
			this.btnKeyDelete.Name = "btnKeyDelete";
			this.btnKeyDelete.Size = new System.Drawing.Size(75, 23);
			this.btnKeyDelete.TabIndex = 8;
			this.btnKeyDelete.Text = "删除";
			this.btnKeyDelete.UseVisualStyleBackColor = true;
			this.btnKeyDelete.Click += new System.EventHandler(this.btnKeyDelete_Click);
			// 
			// btnDelte
			// 
			this.btnDelte.Location = new System.Drawing.Point(360, 26);
			this.btnDelte.Name = "btnDelte";
			this.btnDelte.Size = new System.Drawing.Size(75, 23);
			this.btnDelte.TabIndex = 7;
			this.btnDelte.Text = "删除";
			this.btnDelte.UseVisualStyleBackColor = true;
			this.btnDelte.Click += new System.EventHandler(this.btnDelte_Click);
			// 
			// btnCopyNew
			// 
			this.btnCopyNew.Location = new System.Drawing.Point(279, 27);
			this.btnCopyNew.Name = "btnCopyNew";
			this.btnCopyNew.Size = new System.Drawing.Size(75, 23);
			this.btnCopyNew.TabIndex = 7;
			this.btnCopyNew.Text = "复制";
			this.btnCopyNew.UseVisualStyleBackColor = true;
			this.btnCopyNew.Click += new System.EventHandler(this.btnCopyNew_Click);
			// 
			// KeyId
			// 
			this.KeyId.DataPropertyName = "Id";
			this.KeyId.HeaderText = "主键";
			this.KeyId.Name = "KeyId";
			this.KeyId.Visible = false;
			// 
			// KeyCheck
			// 
			this.KeyCheck.HeaderText = "选中";
			this.KeyCheck.Name = "KeyCheck";
			this.KeyCheck.Visible = false;
			// 
			// KeyText
			// 
			this.KeyText.DataPropertyName = "Key";
			this.KeyText.HeaderText = "关键字";
			this.KeyText.Name = "KeyText";
			this.KeyText.Width = 200;
			// 
			// KeyType
			// 
			this.KeyType.DataPropertyName = "Type";
			this.KeyType.HeaderText = "搜索方向";
			this.KeyType.Items.AddRange(new object[] {
            "横向",
            "竖向"});
			this.KeyType.Name = "KeyType";
			this.KeyType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.KeyType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			// 
			// KeyNum
			// 
			this.KeyNum.DataPropertyName = "Num";
			this.KeyNum.HeaderText = "间隔数量";
			this.KeyNum.Name = "KeyNum";
			// 
			// ConfigFrm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1271, 751);
			this.Controls.Add(this.btnKeyDelete);
			this.Controls.Add(this.btnKeyAdd);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.btnCopyNew);
			this.Controls.Add(this.btnDelte);
			this.Controls.Add(this.btnAdd);
			this.Controls.Add(this.dgvConfigs);
			this.Controls.Add(this.cmbConfigs);
			this.MaximizeBox = false;
			this.Name = "ConfigFrm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigFrm_FormClosing);
			this.Load += new System.EventHandler(this.ConfigFrm_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgvConfigs)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ComboBox cmbConfigs;
		private System.Windows.Forms.DataGridView dgvConfigs;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnKeyAdd;
		private System.Windows.Forms.Button btnKeyDelete;
		private System.Windows.Forms.Button btnDelte;
		private System.Windows.Forms.Button btnCopyNew;
		private System.Windows.Forms.DataGridViewTextBoxColumn KeyId;
		private System.Windows.Forms.DataGridViewCheckBoxColumn KeyCheck;
		private System.Windows.Forms.DataGridViewTextBoxColumn KeyText;
		private System.Windows.Forms.DataGridViewComboBoxColumn KeyType;
		private System.Windows.Forms.DataGridViewTextBoxColumn KeyNum;
	}
}