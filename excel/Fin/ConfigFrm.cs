﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.VisualBasic;

namespace Fin
{
	/// <summary>
	/// 添加配置文件的委托
	/// </summary>
	public delegate void ConfigAdd();
	/// <summary>
	/// 删除配置文件的委托
	/// </summary>
	public delegate void ConfigDeleteed();
	public partial class ConfigFrm : Form
	{
		public event ConfigAdd OnConfigAdd;
		public event ConfigDeleteed OnConfigDeleteed;

		private ConfigRoot _configRoot;
		private List<ConfigKey> _data;
		private Config _typeListItem;

		public ConfigFrm()
		{
			InitializeComponent();

			this.Text = "配置管理";
		}

		#region  窗体事件
		private void ConfigFrm_Load(object sender, EventArgs e)
		{
			Init();

			this.OnConfigAdd += ConfigFrm_OnConfigAdd;
			this.OnConfigDeleteed += ConfigFrm_OnConfigDeleteed;
		}
		private void cmbConfigs_SelectedIndexChanged(object sender, EventArgs e)
		{
			_typeListItem = _configRoot.TypeList.Find(FindTypeListItemByName);
			_data = _typeListItem.Keys;
			this.dgvConfigs.DataSource = _data;

		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(this.cmbConfigs.Text.Trim()))
				{
				MessageBox.Show("配置名字不能为空");
				return;
			}

			ConfigHelper.Create(this.cmbConfigs.Text.Trim(), false);
			Init();

			if (OnConfigAdd != null)
				OnConfigAdd();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			ConfigHelper.Create(_typeListItem.Name, true, _typeListItem);

		}

		private void btnKeyAdd_Click(object sender, EventArgs e)
		{

			this.dgvConfigs.DataSource = null;
			_data.Add(new ConfigKey { });
			this.dgvConfigs.DataSource = _data;
		}

		private void btnKeyDelete_Click(object sender, EventArgs e)
		{


			var fileItems = new List<ConfigKey>();
			foreach (DataGridViewRow itemRow in this.dgvConfigs.SelectedRows)
			{

				foreach (ConfigKey item in _data)
				{
					if ( itemRow.Cells["KeyId"].Value.ToString() == item.Id+"")
					{
						fileItems.Add(item);



					}
				}

			}

			//_data.RemoveAll(delegate (KeysItem item)
			//{
			//	return fileItems.Contains(item);

			//});

			foreach (var item in fileItems)
			{
				_data.Remove(item);
			}

			this.dgvConfigs.DataSource = null;
			this.dgvConfigs.DataSource = _data;

		}

		private void ConfigFrm_FormClosing(object sender, FormClosingEventArgs e)
		{
			this.DialogResult = DialogResult.OK;
		}

		private void btnDelte_Click(object sender, EventArgs e)
		{
			var result = ShowMessageBox();
			if (result == System.Windows.Forms.DialogResult.Yes)
			{
				var re = ConfigHelper.Delete(_typeListItem.Name, true);

				if (re)
				{
					if (OnConfigDeleteed != null)
						OnConfigDeleteed();
				}


			}
		}

		private void btnCopyNew_Click(object sender, EventArgs e)
		{
			var data = _configRoot.TypeList.Find(FindTypeListItemByName);
			if (data == null)
			{
				MessageBox.Show("找不到配置");
				return;
			}
			var defaultName = data.Name +  "-副本";

			string name = Interaction.InputBox("提示信息", "新配置名字", defaultName, -1, -1);

			 
			if (!string.IsNullOrEmpty(name))
			{
				defaultName = name;
				
			}
		  
			
			
			var newTypeListItem = new Config {
				 FileExtension= data.FileExtension,
				  Keys= data.Keys,
				   Name= defaultName,
				    SearchAreaEndKey= data.SearchAreaEndKey,
					 SearchAreaStartKey= data.SearchAreaStartKey,
					  Sort= data.Sort
			};
			 
			ConfigHelper.CopyNew(  newTypeListItem);
			if (OnConfigAdd != null)
				OnConfigAdd();
		}
		#endregion

		#region  窗体委托事件

		private void ConfigFrm_OnConfigDeleteed()
		{
			Init();
			this.dgvConfigs.DataSource = null;
		}

		private void ConfigFrm_OnConfigAdd()
		{
			Init();
		}
		#endregion

		#region 私有自定义方法

		void Init()
		{
			this.dgvConfigs.AutoGenerateColumns = false;
			//this.dgvConfigs.Columns[0].DataPropertyName= "Key";
			//this.dgvConfigs.Columns[1].DataPropertyName = "Type";
			//this.dgvConfigs.Columns[2].DataPropertyName = "Num";
			this.cmbConfigs.Items.Clear();
			_configRoot = ConfigHelper.GetConfig();
			foreach (var item in _configRoot.TypeList)
			{
				this.cmbConfigs.Items.Add(item.Name);
			}
			this.cmbConfigs.Text = "";
			//this.Text = _configRoot.Title;



		}

		private bool FindTypeListItemByName(Config item)
		{
			if (item.Name == this.cmbConfigs.Text)
				return true;
			return false;

		}

		DialogResult ShowMessageBox()
		{


			// Initializes the variables to pass to the MessageBox.Show method.

			string message = InitDefault.ConfigFileDeleteMessage;
			string caption = InitDefault.ConfigFileDeleteTitle;
			MessageBoxButtons buttons = MessageBoxButtons.YesNo;
			DialogResult result;

			// Displays the MessageBox.

			result = MessageBox.Show(message, caption, buttons);
			return result;
			//if (result == System.Windows.Forms.DialogResult.Yes)
			//{

			//	return true;

			//}
			//return false;
		}
		#endregion


	}
}
