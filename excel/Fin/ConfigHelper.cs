﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
namespace Fin
{
	public class ConfigHelper
	{
		public static ConfigRoot GetConfig(bool isConfig = false)
		{
			var re = new ConfigRoot();
			var files = Directory.GetFiles("Configs");

			foreach (var item in files)
			{
				var fileInfo = new FileInfo(item);
				var typeJson = File.ReadAllText(item);
				var typeItem = Newtonsoft.Json.JsonConvert.DeserializeObject<Config>(typeJson);
				if (typeItem != null)
					re.TypeList.Add(typeItem);
			}
			re.TypeList.Sort(TypeListItemComparison);
			return re;
		}
		static int TypeListItemComparison(Config x, Config y)
		{
			return 	x.Name.CompareTo(y.Name);//按银行前缀排序

			if (x.Sort < y.Sort)
				return 1;
			return 0;
		}
		public static Config GetTypeListItemByName(string name)
		{
			foreach (var item in GetConfig().TypeList)
			{
				if (item.Name.Contains(name))
				{
					return item;
				}
			}
			return null;
		}
		public static List<ConfigKey> GetKeyItemsByName(string name)
		{
			foreach (var item in GetConfig().TypeList)
			{
				if (item.Name.Contains(name))
				{
					return item.Keys;
				}
			}
			return null;
		}

		public static bool CopyNew(Config item)
		{

			return Create(item.Name, true, item);
		}

		public static bool Create(string name, bool ifExistsBackAndRemove, Config item)
		{
			if (string.IsNullOrEmpty(name))
				return false;

			try
			{
				var filePathAndName = InitDefault.GetConfigFilePath(name);
				if (File.Exists(filePathAndName))
				{
					if (ifExistsBackAndRemove)
					{
						#region 备份
						BackFile(name);
						#endregion

						File.Delete(filePathAndName);
					}
					else
						return false;
				}
				if (item == null)
					item = TypeListItemHelper.CreateNew(name);

				item.Name = name;
				var json = Newtonsoft.Json.JsonConvert.SerializeObject(item);


				File.AppendAllText(filePathAndName, json);
				return true;
			}
			catch (Exception ex)
			{
				return false;
			}

		}
		private static void BackFile(string name)
		{
			BackFile(name, ConfigFileOptType.Edit);
		}

		/// <summary>
		/// //备份原来的配置文件 
		/// </summary>
		/// <param name="name"></param>
		private static void BackFile(string name, ConfigFileOptType type)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(InitDefault.ConfigsPath);
			var files = directoryInfo.GetFiles("*.json");
			foreach (var file in files)
			{
				if (file.Name.ToLower() == name + ".json")
				{
					file.CopyTo(InitDefault.ConfigsBackPath + Enum.GetName(type.GetType(), type) + "-" + DateTime.Now.ToString("yyyyMMdd-ffff") + "-" +
						 file.Name);
				}

			}

		}

		public static bool Create(string name)
		{
			return Create(name, false, null);

		}

		public static bool Create(string name, bool ifExistsRemove)
		{
			return Create(name, ifExistsRemove, null);

		}

		public static bool Delete(string name, bool back)
		{

			if (back)
			{
				#region 备份
				BackFile(name, ConfigFileOptType.Del);
				#endregion
			}

			try
			{
				File.Delete(InitDefault.GetConfigFilePath(name));

				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
			return false;
		}
	}


	public enum ConfigFileOptType
	{

		Del, Edit
	}
}
