﻿using System;
using System.Collections.Generic;

namespace Fin
{
	public class ConfigKey
	{
		public int Id { get; set; }

		public int ConfigId { get; set; }
		/// <summary>
		/// 数据字段
		/// </summary>
		public string Key { get; set; }

		/// <summary>
		/// 自定义表头
		/// </summary>
		public string HeadName { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string Type { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public int Num { get; set; }


		public ConfigKey() {
			this.Id = 0;
			this.Key = string.Empty;
			this.HeadName = string.Empty;
			this.Type = OrientationDefault.Horizontal;
			this.Num = 0;
		}

	}

	public class Config
	{
		/// <summary>
	 ///  主键
	 /// </summary>
		public int Id { get; set; }
		/// <summary>
		/// 交行（提取关键数据）
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public List<ConfigKey> Keys { get; set; }

		/// <summary>
		/// 排序
		/// </summary>
		public int Sort { get; set; }

		public string FileExtension { get; set; }

		public string SearchAreaStartKey { get; set; }

		public string SearchAreaEndKey { get; set; }

		public Config()
		{
			this.Keys = new List<ConfigKey>();
		}
	}

	public class ConfigRoot
	{
		/// <summary>
		/// 电子表格合并工具
		/// </summary>
		public string Title { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public List<Config> TypeList { get; set; }

		public ConfigRoot()
		{

			this.TypeList = new List<Config>();
		}
	}

	public class OrientationDefault
	{
		public const string Horizontal = "横向";
		public const string Vertical = "竖向";
		 
	}
}
