﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Fin
{
	public	class InitDefault
	{/// <summary>
	 /// 自动更新文件夹
	 /// </summary>
		public static string AutoUpdatePath = AppDomain.CurrentDomain.BaseDirectory + @"AutoUpdate\";
		/// <summary>
		/// 配置文件文件夹路径
		/// </summary>
		public static string ConfigsPath = AppDomain.CurrentDomain.BaseDirectory+ @"Configs\";

		/// <summary>
		/// 备份文件夹路径
		/// </summary>
		public static string ConfigsBackPath = AppDomain.CurrentDomain.BaseDirectory + @"Configs\bak\";

		public const string ConfigFileDeleteTitle = "删除配置文件提示";

		public const string ConfigFileDeleteMessage = "确定删除配置文件吗";

		public static void Init() {

			Fin.Interface.MyDataContext dataContext = new Interface.MyDataContext();

			dataContext.GetConfig();


			try
			{
				if(!System.IO.Directory.Exists(ConfigsBackPath))
					System.IO.Directory.CreateDirectory(ConfigsBackPath);
			}
			catch (Exception ex)
			{

				throw ex;
			}
			return;
			//自动更新
			try
			{
				 

				//启动主程序
				ProcessStartInfo processStartInfo = new ProcessStartInfo(AppDomain.CurrentDomain.BaseDirectory+ @"\AutoUpdate\Autoupdate.exe");

				//processStartInfo.WorkingDirectory = dirInfo.Parent.FullName;


				Process.Start(processStartInfo);
			}
			catch  
			{

				 
			}
		}

		public static string GetConfigFilePath(string name) {
			return ConfigsPath + name + ".json";

		}
	}
}
