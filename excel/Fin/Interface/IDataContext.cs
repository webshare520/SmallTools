﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fin.Interface
{
	public interface IDataContext
	{
		ConfigRoot GetConfig(bool isConfig = false);

		Config GetTypeListItemByName(string name);

		List<ConfigKey> GetKeyItemsByName(string name);

		bool CopyNew(Config item);


		bool Create(string name, bool ifExistsBackAndRemove, Config item);

		bool Delete(string name, bool back);
	}
}
