﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fin.Interface
{
	public interface IUnitOfWork
	{
		void Save();
	}
}
