﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Fin
{
    public class KeyCopyHandler : BaseHandler
    {
        public KeyCopyHandler(Config typeListItem) : base(typeListItem)
        { }

        public override BaseHandler SumSheet()
        {

            #region 创建表头
            var headRow = _sheet.CreateRow(0);
            for (int i = 0; i < _typeListItem.Keys.Count; i++)
            {
                headRow.CreateCell(i).SetCellValue(_typeListItem.Keys[i].Key + ":" + _typeListItem.Keys[i].Num);
            }
            #endregion

            foreach (var file in Files)
            {
                if (_typeListItem.FileExtension.ToLower().Contains(file.Extension.ToLower()))
                {
                    using (var fs = File.Open(file.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        IWorkbook workbook;
                        if (file.Name.ToLower().Contains(".xlsx"))
                            workbook = new XSSFWorkbook(fs);
                        else if (file.Extension.ToLower().Contains(".csv"))
                        {
                            workbook = CSVHelper.GetWorkbook(file.FullName);
                        }
                        else
                        {
                            workbook = new HSSFWorkbook(fs);
                        }
                        ISheet sheet = workbook.GetSheetAt(0);

                        var rows = sheet.LastRowNum;




                     
                        var datas = NpoiExt.GetSheetDatas(sheet);

                       

                        if (_typeListItem.Keys.Count > 0) {
                            var resultDatas = GetRowData(datas, _typeListItem.Keys);
                            var newRow = _sheet.CreateRow(_sheet.LastRowNum + 1);
                            for (int i = 0; i < resultDatas.Count; i++)
                            {
                                var value = resultDatas[i];
                                newRow.CreateCell(i).SetCellValue(value);
                            }
                        }

                        else
                        {
                            for (int i = 0; i < datas.Count; i++)
                            {
                                var newRow = _sheet.CreateRow(_sheet.LastRowNum + 1);
                                var row = datas[i];
                                for (int j = 0; j < row.Count; j++)
                                {
                                    var value = row[j];
                                    newRow.CreateCell(j).SetCellValue(value);

                                }
                            }

                        }

                        
                        

                    }
                }
            }

            return this;
        }


        private List<string> GetRowData(List<List<string>> datas, List<ConfigKey> keysItems)
        {
            var re = new List<string>();



            foreach (var keyItem in keysItems)
            {
                if (keyItem.Type == OrientationDefault.Horizontal)
                {
                    foreach (var rowItem in datas)
                    {
                        int keyIndex = 0;

                        if (rowItem.Contains(keyItem.Key))
                        { 
                            keyIndex = rowItem.IndexOf(keyItem.Key);

							if (rowItem.Count > (keyIndex + keyItem.Num))
								re.Add(rowItem[keyIndex+keyItem.Num]);
                        }


                    }
                }
                else if (keyItem.Type == OrientationDefault.Vertical)
                {
                    int rowIndex = 0;
                    int columnIndex = 0;
                    for (int i = 0; i < datas.Count; i++)
                    {
                        var row = datas[i];
                        if (row.Contains(keyItem.Key))
                        {
                            rowIndex = i;
                            columnIndex = row.IndexOf(keyItem.Key);

                            re.Add(datas[i+keyItem.Num][columnIndex]);
                        }

                    }
                   

                     
                }
            }



            return re;

        }
        
    }
}
