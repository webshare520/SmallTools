﻿/*
 * 由SharpDevelop创建。
 * 用户： Administrator
 * 日期: 2017/10/17
 * 时间: 15:05
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Net;
using System.Threading;
using System.Diagnostics;

namespace Fin
{

	delegate void GetRowsHandler(string path);

	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{ 
		private BaseHandler _handler;
		private Config _typeListItem;

		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			 
			//saveFileDialog1.DefaultExt = "*.xls;*xlsx";

			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		void BtnChooseDirClick(object sender, EventArgs e)
		{

			var show = this.folderBrowserDialog1.ShowDialog();
			if (show == DialogResult.OK)
			{

                 

               
                if (_handler != null)
					_handler.SetPath(folderBrowserDialog1.SelectedPath)
						.SumSheet()
						.SaveAndOpent();

			}

		}



		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.comboBox1.Text == "完全拷贝")
			{
				_typeListItem = ConfigHelper.GetTypeListItemByName(comboBox1.Text);
				_handler = new BaseHandler(_typeListItem);
			}
			else
			{
				_typeListItem = ConfigHelper.GetTypeListItemByName(comboBox1.Text);

				_handler = new KeyCopyHandler(_typeListItem);

			}
		}

		private void CheckVersion() {
			ProcessStartInfo startInfo = new ProcessStartInfo(@"AutoUpdate\Autoupdate.exe");
			startInfo.WindowStyle = ProcessWindowStyle.Minimized;

			Process.Start(startInfo);
			//var process= Process.Start(System.Environment.CurrentDirectory+@"\AutoUpdate\Autoupdate.exe");

			Thread.Sleep(200);
		}

		void Init()
		{
		 
			this.comboBox1.Items.Clear();
			var  configRoot = ConfigHelper.GetConfig();
			foreach (var item in configRoot.TypeList)
			{
				this.comboBox1.Items.Add(item.Name);
			}
			this.comboBox1.Text = "";
			//this.Text = configRoot.Title;
		}


		private void MainForm_Load(object sender, EventArgs e)
		{
			#region 检查更新
			//if(File.Exists(@"AutoUpdate\Autoupdate.exe"))
			//Process.Start(@"AutoUpdate\Autoupdate.exe");
			//this.backgroundWorker1.DoWork += BackgroundWorker1_DoWork;
			//this.backgroundWorker1.RunWorkerCompleted += BackgroundWorker1_RunWorkerCompleted;
			//this.backgroundWorker1.RunWorkerAsync();


			//CheckVersion();
			#endregion

			Init();
		}

		private void BackgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
		{
			//throw new NotImplementedException();
		}

		private void BackgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
		{
			CheckVersion();
		}

		private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			var frm = new ConfigFrm(); 
			if (frm.ShowDialog(this) == DialogResult.OK)
			{
				MainForm_Load(this,null);
			}
		}
		 
	}

}
